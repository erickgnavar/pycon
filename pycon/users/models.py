from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.postgres.fields import CITextField
from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel

from .managers import UserManager


class User(PermissionsMixin, TimeStampedModel, AbstractBaseUser):
    email = CITextField(verbose_name=_("Email address"), max_length=255, unique=True,)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    first_name = models.CharField(_("First name"), max_length=100, null=True)
    last_name = models.CharField(_("Last name"), max_length=100, null=True)

    class Meta:
        db_table = "auth_user"

    def __str__(self):
        return self.email

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin
