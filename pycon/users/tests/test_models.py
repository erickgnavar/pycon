from django.test import TestCase

from ..models import User


class UserTestCase(TestCase):
    def test_str(self):
        user = User(email="demo")
        self.assertEqual(str(user), "demo")

    def test_full_name(self):
        user = User(first_name="first", last_name="last")
        self.assertEqual(user.full_name, "first last")
