from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel

from . import enums


class Category(TimeStampedModel):

    name = models.CharField(_("Name"), max_length=30)
    slug = models.SlugField(editable=False, max_length=40)

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")
        default_related_name = "events"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super().save(*args, **kwargs)


class Event(TimeStampedModel):

    title = models.CharField(_("Title"), max_length=200)
    slug = models.SlugField(editable=False, max_length=250)
    description = models.TextField(_("Description"))

    date = models.DateField(_("Date"))
    time = models.TimeField(_("Time"))
    duration = models.PositiveSmallIntegerField(
        _("Duration"), help_text=_("In minutes")
    )

    level = models.CharField(
        _("Level"),
        max_length=20,
        choices=enums.Level.choices,
        default=enums.Level.BEGINNER,
    )
    type = models.CharField(
        _("Type"),
        max_length=20,
        choices=enums.EventType.choices,
        default=enums.EventType.TALK,
    )

    category = models.ForeignKey("Category", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        default_related_name = "events"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super().save(*args, **kwargs)
