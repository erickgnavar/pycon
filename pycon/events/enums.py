from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


class Level(TextChoices):
    BEGINNER = "BEGINNER", _("Beginner")
    INTERMEDIATE = "INTERMEDIATE", _("Intermediate")
    ADVANCED = "ADVANCED", _("Advanced")


class EventType(TextChoices):

    TALK = "TALK", _("Talk")
    BREAK = "BREAK", _("Break")
    KEYNOTE = "KEYNOTE", _("Keynote")
