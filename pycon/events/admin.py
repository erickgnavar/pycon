from django.contrib import admin

from .models import Category, Event


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):

    list_display = ("name",)
    search_fields = ("name",)


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):

    list_display = ("title", "date", "time")
    search_fields = ("title",)
    list_filter = ("level", "type", "category")
    autocomplete_fields = ("category",)
