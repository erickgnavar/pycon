from django.test import TestCase

from ..models import Category, Event


class CategoryTestCase(TestCase):
    def test_str(self):
        category = Category(name="demo")
        self.assertEqual(str(category), "demo")


class EventTestCase(TestCase):
    def test_str(self):
        event = Event(title="demo")
        self.assertEqual(str(event), "demo")
