from rest_framework.routers import DefaultRouter

from . import views

app_name = "apiv1"

router = DefaultRouter()
router.register(r"categories", views.CategoryViewSet, basename="categories")
router.register(r"events", views.EventViewSet, basename="events")
urlpatterns = router.urls
