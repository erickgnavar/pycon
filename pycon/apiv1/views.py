from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from pycon.events.models import Category, Event

from . import serializers


class CategoryViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Category.objects.all()
        serializer = serializers.CategorySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Category.objects.all()
        category = get_object_or_404(queryset, pk=pk)
        serializer = serializers.CategorySerializer(category)
        return Response(serializer.data)


class EventViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Event.objects.all()
        serializer = serializers.EventSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Event.objects.all()
        event = get_object_or_404(queryset, pk=pk)
        serializer = serializers.EventSerializer(event)
        return Response(serializer.data)
